module.exports = {
  map: true,
  plugins: {
    /** Use first so that other plugins are applied after import. */
    'postcss-import': {} 
  
  , /** Provides: variables, extends, and property-lookup. */
    'precss': {}
  }
}