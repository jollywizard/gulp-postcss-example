const gulp = require('gulp');
const postcss = require('gulp-postcss');
const browserSync = require('browser-sync').create();

const target = './dest'

gulp.task('copy-static', function () {
  return gulp.src('./src/*.html')
     .pipe(gulp.dest(target));
})

gulp.task('build-css', function () {
  return gulp.src('./src/index.css')
    .pipe(postcss())
    .pipe(gulp.dest(target));
});

gulp.task('build', ['copy-static', 'build-css'], () => {} )

gulp.task('watch', ['build'], () => {
  gulp.watch('src/**', ['build'])
})

gulp.task('serve', ['watch'], () => {
  browserSync.init({
    server: target
  })

  gulp.watch(target + '/**')
    .on('change', browserSync.reload)
})

gulp.task('default', ['serve'])
