# gulp-postcss-example

Demonstrates how to use `gulp` to compile `postcss` into servable css.

Browsersync hosts/syncs compiled resources locally.

## Usage

`npm install` : to install devDependencies.

`npm start` | `gulp` | `gulp serve` : build / sync / host files @ `localhost:3000`.

## Postcss Plugins

The following plugins are enabled in `postcss.config.js`:

  * `postcss-import` : Build files from modules using `@import`
  * `precss` : Provides equivalent features from other compile-to-css frameworks.
    * `@extend` : for inherited properties and derivative selectors.
    * property lookup : for property reuse. e.g. `height:@width` or `height: calc(@min-width + 1em)`
    * `$variables` : for value substitution / reuse.
  